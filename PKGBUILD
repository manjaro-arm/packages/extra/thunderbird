# Maintainer: Philip Müller <philm at manjaro dot org>
# Maintainer: Helmut Stult <helmut at manjaro dot org>

# Arch credits:
# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Alexander Baldeck <alexander@archlinux.org>
# Contributor: Dale Blount <dale@archlinux.org>
# Contributor: Anders Bostrom <anders.bostrom@home.se>

# Cloudbuild Server
_server=cpx51

pkgname=thunderbird
pkgver=78.6.0
pkgrel=1.1
pkgdesc='Standalone mail and news reader from mozilla.org'
url='https://www.mozilla.org/thunderbird/'
arch=(x86_64 aarch64)
license=(MPL GPL LGPL)
depends=(
  glibc gtk3 libgdk-3.so mime-types dbus libdbus-1.so dbus-glib alsa-lib nss
  hunspell sqlite ttf-font libvpx libvpx.so zlib bzip2 botan libwebp libevent
  libjpeg-turbo libffi nspr gcc-libs libx11 libxrender libxfixes libxext
  libxcomposite libxdamage pango libpango-1.0.so cairo gdk-pixbuf2 icu
  libicui18n.so libicuuc.so freetype2 libfreetype.so fontconfig
  libfontconfig.so glib2 libglib-2.0.so pixman libpixman-1.so gnupg
)
makedepends=(
  unzip zip diffutils python python-setuptools yasm nasm mesa imake libpulse
  inetutils xorg-server-xvfb autoconf2.13 rust clang llvm gtk2 cbindgen nodejs
  gawk perl findutils
)
optdepends=('libcanberra: sound support')
options=(!emptydirs !makeflags)
source=(https://ftp.mozilla.org/pub/mozilla.org/thunderbird/releases/$pkgver/source/thunderbird-$pkgver.source.tar.xz
        thunderbird.desktop
        vendor-prefs.js
        distribution.ini
        mozconfig.cfg
        configure-fix-passing-system-bzip2-ldflags.patch
        thunderbird-rust-1.47.patch::https://src.fedoraproject.org/rpms/thunderbird/raw/86967ddc206310ba7bb9eb57a933031909288ce2/f/rust-1.47.patch)
sha512sums=('36194e8bf58117d8accbd6d8dc52a6241d8c572c023db1b271db3b73098652a608da28134865099792fbeb0a2f0d3705d98093447b64fa19eab7efb3e3bdd421'
            'a0061fcb2a7f66061e336a8d95948592f56f4752e56467f14ba63846720ebf845cce7511d1a2637e3b80d5a1ffdaa2fb783fa37195103425ef65222d45372012'
            '6918c0de63deeddc6f53b9ba331390556c12e0d649cf54587dfaabb98b32d6a597b63cf02809c7c58b15501720455a724d527375a8fb9d757ccca57460320734'
            'cb4bd44d309b76f7ccdf175c57fc15d390358c48e8fa56ea986d727028ee83fcfbe67b28839cfa85f5c0454feb24a70708dabc306170f0c057bbe3ef086f91d3'
            'a777ce59bafabe641104a8b1d7c519bede752110ca25e0daa76a7e4e44eee2ce3c226ca793ea4f0b92c89b6cc91076197c9c45d3633bebab34a12d9eded3cd54'
            'db9075ddbe715d8d542558ab3ec345545ac2607eb71807a7dcb3ac168123eb0bb8dd91a718bfb207e43224951ec20c363c60ea4d4202df0f11f498467e372494'
            'd337a77104d411df219f3ae1c7d136ae92f944a18969cc92e6257b4f909204677a58df43187d41f53d4c36c29f57f53bdbad90d8263e6a9b0781640944895cad')

# Google API keys (see http://www.chromium.org/developers/how-tos/api-keys)
# Note: These are for Manjaro Linux use ONLY. For your own distribution, please
# get your own set of keys. Feel free to contact helmut@manjaro.org for
# more information.
_google_api_key=AIzaSyCJysf0qByNCVmZhH4cE1LxPZ20hJT3ipg

# Mozilla API keys (see https://location.services.mozilla.com/api)
# Note: These are for Manjaro Linux use ONLY. For your own distribution, please
# get your own set of keys. Feel free to contact helmut@manjaro.org for
# more information.
_mozilla_api_key=27e30dd8-ba74-4ef0-86c6-9fceab8a7089

prepare() {
  cd $pkgname-$pkgver

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done

  printf "%s" "$_google_api_key" >google-api-key
  printf "%s" "$_mozilla_api_key" >mozilla-api-key
  cp ../mozconfig.cfg .mozconfig
  sed "s|@PWD@|${PWD@Q}|g" -i .mozconfig

  echo "mk_add_options MOZ_MAKE_FLAGS=\"${MAKEFLAGS}\"" >> .mozconfig
  export MOZ_DEBUG_FLAGS=" "
  export CFLAGS+=" -g0"
  export CXXFLAGS+=" -g0"
  export LDFLAGS+=" -Wl,--no-keep-memory -Wl,--reduce-memory-overheads"
  export RUSTFLAGS="-Cdebuginfo=0"
}

build() {
  cd $pkgname-$pkgver
  export MOZ_BUILD_DATE=$(head -1 sourcestamp.txt)

  ./mach configure
  ./mach build
  ./mach buildsymbols
}

package() {
  cd $pkgname-$pkgver
  DESTDIR="$pkgdir" ./mach install

  install -Dm 644 ../vendor-prefs.js -t "$pkgdir/usr/lib/$pkgname/defaults/pref"
  install -Dm 644 ../distribution.ini -t "$pkgdir/usr/lib/$pkgname/distribution"
  install -Dm 644 ../thunderbird.desktop -t "$pkgdir/usr/share/applications"

  for i in 16 22 24 32 48 64 128 256; do
    install -Dm644 comm/mail/branding/thunderbird/default${i}.png \
      "$pkgdir/usr/share/icons/hicolor/${i}x${i}/apps/$pkgname.png"
  done
  install -Dm644 comm/mail/branding/thunderbird/TB-symbolic.svg \
    "$pkgdir/usr/share/icons/hicolor/symbolic/apps/thunderbird-symbolic.svg"

  # Use system-provided dictionaries
  ln -Ts /usr/share/hunspell "$pkgdir/usr/lib/$pkgname/dictionaries"
  ln -Ts /usr/share/hyphen "$pkgdir/usr/lib/$pkgname/hyphenation"

  # Install a wrapper to avoid confusion about binary path
  install -Dm755 /dev/stdin "$pkgdir/usr/bin/$pkgname" <<END
#!/bin/sh
exec /usr/lib/$pkgname/thunderbird "\$@"
END

  # Replace duplicate binary with wrapper
  # https://bugzilla.mozilla.org/show_bug.cgi?id=658850
  ln -srf "$pkgdir/usr/bin/$pkgname" \
    "$pkgdir/usr/lib/$pkgname/thunderbird-bin"
}
